import { createApp } from 'vue'
import App from './App.vue'
import './../node_modules/bulma/css/bulma.css';

import { library } from '@fortawesome/fontawesome-svg-core';

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import { faShoppingCart, faArrowCircleUp, faArrowCircleDown, faTrash, faUserCircle, faUsd } from '@fortawesome/free-solid-svg-icons';
import store from './store';

library.add(faShoppingCart, faArrowCircleUp, faArrowCircleDown, faTrash, faUserCircle, faUsd);

// Vue.component('font-awesome-icon', FontAwesomeIcon);

createApp(App)
	.component('font-awesome-icon', FontAwesomeIcon)
	.use(store)
	.mount('#app')
