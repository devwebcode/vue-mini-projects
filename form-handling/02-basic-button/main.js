const ButtonRow = {
	template: `
		<div>
			<button name="button-hoodie" value="fullstack-hoodie"
				@click="onButtonClick" class="ui inverted button">Hoodie</button>
			<button name="button-tee" value="fullstack-tee"
				@click="onButtonClick" class="ui inverted button">Tee</button>
			<button name="button-fitted-cap" value="fullstack-fitted-cap"
				@click="onButtonClick" class="ui inverted button">FittedCap</button>
			<button name="button-jacket" value="fullstack-jacket"
				@click="onButtonClick" class="ui inverted button">Jacket</button>
		</div>`,
	methods: {
		onButtonClick(e) {
			const button = e.target;
			console.log(`The user clicked ${button.name}: ${button.value}`);
		},
	},
}

Vue.createApp({
	components: {
		"button-row": ButtonRow,
	},
}).mount("#app");
