const ButtonRow = {
	template: `
		<div>
			<button @click="onHoodieClick" class="ui inverted button">Hoodie</button>
			<button @click="onTeeClick" class="ui inverted button">Tee</button>
			<button @click="onFittedCapClick" class="ui inverted button">FittedCap</button>
			<button @click="onJacketClick" class="ui inverted button">Jacket</button>
		</div>`,
	methods: {
		onHoodieClick(e) {
			console.log("user clicked hoodie button", e);
		},
		onTeeClick(e) {
			console.log("user clicked tee button", e);
		},
		onFittedCapClick(e) {
			console.log("user clicked cap button", e);
		},
		onJacketClick(e) {
			console.log("user clicked jacket button", e);
		},
	},
}

Vue.createApp({
	components: {
		"button-row": ButtonRow,
	},
}).mount("#app");
