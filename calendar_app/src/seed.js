export const seedData = [
	{
		id: 1,
		abbvTitle: 'Mon',
		fullTitle: 'Monday',
		events: [
			{ details: 'Program Rust', edit: false },
			{ details: 'Bash string manipulation', edit: false },
		],
		active: true,
	},
	{
		id: 2,
		abbvTitle: 'Tue',
		fullTitle: 'Tuesday',
		events: [
			{ details: 'Learn Vue', edit: false },
		],
		active: false,
	},
	{
		id: 3,
		abbvTitle: 'Wed',
		fullTitle: 'Wednesday',
		events: [
			{ details: 'Competitive Coding C++', edit: false },
		],
		active: false,
	},
	{
		id: 4,
		abbvTitle: 'Thu',
		fullTitle: 'Thursday',
		events: [ ],
		active: false,
	},
	{
		id: 5,
		abbvTitle: 'Fri',
		fullTitle: 'Friday',
		events: [
			{ details: 'Reactive Spring', edit: false },
		],
		active: false,
	},
	{
		id: 6,
		abbvTitle: 'Sat',
		fullTitle: 'Saturday',
		events: [
			{ details: 'Concurrency with Go', edit: false },
		],
		active: false,
	},
	{
		id: 7,
		abbvTitle: 'Sun',
		fullTitle: 'Sunday',
		events: [
			{ details: 'Android Development', edit: false },
		],
		active: false,
	}
]
