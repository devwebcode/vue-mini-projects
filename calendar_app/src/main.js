import { createApp } from 'vue'
import App from './App.vue'
import './../node_modules/bulma/css/bulma.css';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faUserSecret, faPencil, faTrash, faCheck } from '@fortawesome/free-solid-svg-icons';

library.add(faUserSecret, faPencil, faTrash, faCheck);

createApp(App)
	.component('font-awesome-icon', FontAwesomeIcon)
	.mount('#app')
