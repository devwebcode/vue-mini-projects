import { seedData } from './seed.js'
import { reactive } from 'vue';

export const store = {
	state: {
		data: reactive(seedData),
	},

	getActiveDay() {
		return this.state.data.find((day) => day.active)
	},

	setActiveDay(dayId) {
		this.state.data.map((day) => {
			day.id === dayId ? day.active = true : day.active = false;
		});
	},

	submitEvent(eventDetails) {
		const activeDay = this.getActiveDay();
		activeDay.events.push({
			"details": eventDetails,
			"edit": false,
		});
	},

	getEventObj(dayId, eventDetails) {
		const dayObj = this.state.data.find(day => day.id === dayId);
		return dayObj.events.find(event => event.details === eventDetails);
	},

	editEvent(dayId, eventDetails) {
		this.resetEditOfAllEvents();  // reset other events edit first

		const eventObj = this.getEventObj(dayId, eventDetails);
		eventObj.edit = true;
	},

	resetEditOfAllEvents() {
		this.state.data.map(day => {
			day.events.map(event => event.edit = false);
		});
	},

	updateEvents(dayId, eventDetails, newEventDetails) {
		const eventObj = this.getEventObj(dayId, eventDetails);
		eventObj.details = newEventDetails;
		eventObj.edit = false;
	},

	deleteEvent(dayId, eventDetails) {
		const dayObj = this.state.data.find(day => day.id === dayId);
		const eventIndexToRemove = dayObj.events.findIndex(event => event.details === eventDetails);
		dayObj.events.splice(eventIndexToRemove, 1);
	}
}
